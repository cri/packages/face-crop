from PIL import Image
import face_recognition
import numpy as np


def to_int_tuple(*arrays):
    return tuple(np.round(np.concatenate(arrays)).astype("int"))


MAX_SIZE = 1000


def crop_image(image, zoom=1.0, center=0.5, size=(500, 500)):
    """
    Detect face on image and crop around it

    Arguments :
    image -- PIL image
    zoom -- Zoom factor
    center -- Offset on the y axis, where 1 is the distance between eyes and mouth
    size -- Size of the output image (tuple width, height)
    """
    scale = 1.0
    image_to_process = image

    if np.max(image.size) > MAX_SIZE:
        # The face recognition algorithm doesn't seem to do as well on very big
        # images, so we pass it a scaled down version.
        downsampled = image.copy()
        scale = MAX_SIZE / np.max(downsampled.size)
        downsampled = downsampled.resize(
            to_int_tuple(np.array(downsampled.size) * scale), resample=Image.BICUBIC
        )
        image_to_process = downsampled

    arr_image = np.array(image_to_process)
    faces = face_recognition.face_landmarks(arr_image)

    if len(faces) != 1:
        raise Exception(f"Found {len(faces)}, expected 1")

    face = faces[0]

    # Centroid of eyes and mouth
    eyes = np.mean(face["left_eye"] + face["right_eye"], axis=0) / scale
    mouth = np.mean(face["bottom_lip"] + face["top_lip"], axis=0) / scale

    # Heuristic for finding the face "center"
    unit_vector = eyes - mouth
    face_centroid = eyes * 0.9 + mouth * 0.1 + unit_vector * (0.5 - center)

    # Heuristic for determining the radius of the ellipse encircling the face
    r = np.linalg.norm(eyes - mouth) * 2
    # draw_circle(d, face_centroid, r, fill=None, outline=(255, 0, 0, 100))

    # Computing the boundaries of the ellipse centered on the face
    ratio = np.array(size) / np.max(size)
    dimensions = np.array([r * 2 / zoom, r * 2 / zoom]) * ratio
    top_left = face_centroid - dimensions / 2
    bottom_right = face_centroid + dimensions / 2

    # Make sure we're within the image's bounding box
    top_left_fixed = np.fmax([0, 0], top_left)
    bottom_right_fixed = np.fmin(image.size, bottom_right)

    # The previous operation might have incurred an offset
    offset = top_left_fixed - top_left
    real_dimensions = bottom_right_fixed - top_left_fixed

    real_ratio = real_dimensions / np.max(real_dimensions)

    rc = image.crop(to_int_tuple(top_left_fixed, bottom_right_fixed))

    # If the image was too zoomed in for our purpose, we actually need to scale
    # it down so it is still centered and normalized.
    actual_size = np.max(size * real_dimensions / dimensions)

    # Keep the ratio, but set the maximum dimension to `actual_size`
    resize_dimensions = real_ratio * actual_size

    # Find the offset that's needed in order to center the face
    resize_offset = offset / np.max(real_dimensions) * actual_size

    rc = rc.resize(to_int_tuple(resize_dimensions), resample=Image.BICUBIC)

    # Paste the image onto a blank background.
    bg = Image.new("RGB", size, (255, 255, 255))

    bg.paste(rc, to_int_tuple(resize_offset))

    return bg
